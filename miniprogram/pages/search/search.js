// miniprogram/pages/search/search.js
const db = wx.cloud.database()
const _=db.command
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name:"",
    school:[],
    hidden:true

  },

  getData(){
    let name=this.data.name
    db.collection('schoolList')
    .where({
      name:name
    })
    .orderBy('name','asc')
    .get().then((res)=>{
      // console.log(res)
      this.setData({
        school:res.data
      })
      if(this.data.school.length==0){
        this.setData({
          hidden:false
        })
      }
      
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var name=options.name
    // console.log(name)
    this.setData({
      name:name
    })
    // console.log(this.data.name)
    this.getData()

    // const eventChannel = this.getOpenerEventChannel()
    // eventChannel.on('getname', function(data) {
    //   this.setData
    // })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },



  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})