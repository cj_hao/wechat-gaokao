// const db = wx.cloud.database()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    swiperList: [],
    active: 0,
    tabIndex: 0,
    value: ''
  },

  //搜索框
  inChange(e) {
    // console.log(e)
    this.setData({
      value: e.detail,
    });
  },
  onSearch() {
    // console.log(this.data.value);
    var name=this.data.value
    wx.navigateTo({
      url: '/pages/search/search?name='+name,  
      // success: function(res) {
      //   // 通过eventChannel向被打开页面传送数据
      //   res.eventChannel.emit('getname', { data: name})
      // }     
      })

  },
  onClick() {
    // console.log('搜索' + this.data.value);
    var name=this.data.value
    wx.navigateTo({
      url: '/pages/search/search?name='+name,
      // success: function(res) {
      //   // 通过eventChannel向被打开页面传送数据
      //   res.eventChannel.emit('getname', { data: name })
      // }        
      })
  },


  // add(num1=1,num2=2){
  //   var num=num1+num2
  //   console.log(num)
  // },

  //加载轮播图片
  loadswiper() {
    wx.cloud.callFunction({
      name :"getswiperList"
    }).then(res=>{
      // console.log (res)
      this.setData({
        swiperList:res.result.data
      })
    })
    // db.collection('swiperList').get(
    //   {
    //     success: res => {
    //       // console.log(res.data)
    //       this.setData({
    //         //将从云端获取的数据放到testList中
    //         swiperList: res.data,
    //       })

    //     },
    //     fail: console.error
    //   }
    // )
  },
//tab标签页切换
  onChange(event) {
    this.setData({
      tabIndex : event.detail.name
    })
    // console.log(this.data.tabIndex)
    // wx.showToast({
    //   title: `切换到标签 ${event.detail.name}`,
    //   icon: 'none',
    // });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.loadswiper();
    // this.add();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    // console.log("触底事件")
    switch(this.data.tabIndex){
      case 0:
        // this.score=this.selectComponent("#score")
        // this.score.refreshList()
        // console.log("这是第一个页面")
        break;
      case 1:
        // console.log("这是第二个页面")
        break;
      case 2:        
        this.list=this.selectComponent("#list")
        this.list.refreshList()
        // console.log("这是第三个页面")
        break;
      default:
        console.log("无效")
    }
    

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})