// components/list/list.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },
  lifetimes: {
    // attached: function() {
    //   console.log("组件被创建")
    // },
    ready: function() {
      this.getList();
    },
  },
  /**
   * 组件的初始数据
   */
  data: {
    rankList:[],

    option1: [
      { text: '理科', value: 0 },
      { text: '文科', value: 1 },
    ],
    value1: 0,
    type:'sscore',
    hidden:false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    getList(type='sscore') {
      // var num=num1+num2
      // console.log (num)
      wx.cloud.callFunction({
        name :"getrankList",
        data:{
          num:5,
          page:0,
          type
        }
      }).then(res=>{
        // console.log(this.data.ranklist.length)
        // var oldData=this.data.unilist
        // var newData=oldData.concat(res.result.data);
        this.setData({
          ranklist: res.result.data
        })
        // console.log(res)       
      })    
    },
    //拼接新旧数组
    conList(num,page,type) {
      wx.cloud.callFunction({
        name :"getrankList",
        data:{
          num:num,
          page:page,
          type:type
        }
      }).then(res=>{
        // console.log(this.data.unilist.length)
        var oldData=this.data.ranklist
        var newData=oldData.concat(res.result.data);
        this.setData({
          ranklist: newData
        })       
      })    
    },
    //底部刷新数组
    refreshList(){
      let page=this.data.ranklist.length
      let type=this.data.type
      this.conList(5,page,type)
      // console.log(this.data.ranklist.length)
    },

    onChange(value){
      // console.log(value.detail)
      var a=value.detail
      // console.log(a)
      if(a==0){
        this.setData({
          type:"sscore",
          hidden:!this.data.hidden
        })
        this.getList('sscore')
        // console.log(this.data.type)
      }else{
        this.setData({
          type :"lscore",
          hidden:!this.data.hidden
        })
        this.getList('lscore')
        // console.log(this.data.type)
    }

  },
}
})
