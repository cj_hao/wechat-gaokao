
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },
  // lifetimes: {
  //   // attached: function() {
  //   //   console.log("组件被创建")
  //   // },
  //   ready: function() {
  //     this.query();
  //   },
  // },

  /**
   * 组件的初始数据
   */
  data: {
    isChecked:true,
    value:0,
    type:"理科",
    rank:0,
    score:0,
    scoreList:[],
    minrank:0,
    minrankList:[],
    schoolList:[],
    hidden:false,
    thhidden:true
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // formSubmit: function (e) {
    //   this.setData({
    //     rank:e.detail.value.rank
    //   })
    //   console.log(this.data.rank)
    //  },

    //正则表达式替换特殊字符
    replaceInput(e){
      // console.log(e.detail.value)
      let value= e.detail.value.replace(/\D/g,'')
      this.setData({
        rank:value
      })
      // console.log(this.data.inputvalue)
      return this.data.rank
    },

    //更换样式
    changeClass: function(event){  
       var a=this.data.value
       var b=event.currentTarget.dataset.value
      //  console.log(a,b)
       if(a!==b){
        this.setData({
          isChecked:!this.data.isChecked,
          value:b
        })
       }          
       if(this.data.value==0){
        this.setData({
          type:"理科"
        })
       }else{
        this.setData({
          type:"文科"
        })
      }
      // console.log(this.data.type)
    },

    //设置分数范围
    setscoreList(){
      let a=parseInt(this.data.score)
      let num=5
      console.log(a)
      console.log(this.data.type)
      this.setData({
        scoreList:this.data.scoreList.concat(a+num,a,a-num,a-2*num)
      })
      console.log(this.data.scoreList)
    },

    //获取排位区间
    getranklist(type,score){
      let p = new Promise(function(resolve, reject){
        wx.cloud.callFunction({
          name: "getrank",
          data: {
            type: type,
            score: score
          }
        }).then(res=>{
          // console.log(score)
          let a = res.result.data[0].minrank
           resolve(a)
          })
        })      
         return p
       },

    //查理科学校
    getsschool(rank1,rank2){
      let p = new Promise(function(resolve, reject){
        wx.cloud.callFunction({
          name: "getsschool",
          data: {
            rank1,
            rank2
          }
        }).then(res=>{
          // console.log(res)
          let array=res.result.data
          resolve(array)
          })
        })      
         return p
       },
    //查文科学校
    getlschool(rank1,rank2){
    let p = new Promise(function(resolve, reject){
      wx.cloud.callFunction({
        name: "getlschool",
        data: {
          rank1,
          rank2
        }
      }).then(res=>{
        // console.log(res)
        let array=res.result.data
        resolve(array)
        })
      })      
        return p
      },

    //通过排位组查询学校
    queryschool(){
      let that=this
      let a=that.data.minrankList
      let type=that.data.type
      if(type=="理科"){
        that.getsschool(a[0],a[3]).then(res=>{
          // console.log(res)
          let schoolList=that.forslist(res,a)
          this.setData({
            schoolList:schoolList
          })
          // that.setData({
          //   schoolList:res
          // })
          console.log(that.data.schoolList)
        })
      }else{
        that.getlschool(a[0],a[3]).then(res=>{
          // console.log(res)
          let schoolList=that.forwlist(res,a)
          this.setData({
            schoolList:schoolList
          })
          // that.setData({
          //   schoolList:res
          // })
          console.log(that.data.schoolList)
          
        })
      }
    },

    //遍历数组,加字段
    forslist(school,a){
      let that=this
      school.forEach(item => {
        if(item.srank>=a[0]&&item.srank<a[1]){
          item.advice = "冲刺";
        }else if(item.srank>=a[1]&&item.srank<a[2]){
          item.advice="稳妥";
        }else{
          item.advice="保底";
        }         
      })
      this.setData({
        hidden:false
      })
      return school 
    },
    forwlist(school,a){
      let that=this
      school.forEach(item => {
        if(item.lrank>=a[0]&&item.lrank<a[1]){
          item.advice = "冲";
        }else if(item.lrank>=a[1]&&item.lrank<a[2]){
          item.advice="稳";
        }else{
          item.advice="保";
        }         
      })
        this.setData({
          hidden:true
        })
      return school 
    },

    getinit(){
      // console.log('这是get方法')
      let that=this
      this.setscoreList();
      let a=that.data.scoreList
      let type=that.data.type
      Promise.all([that.getranklist(type,a[0]),that.getranklist(type,a[1]),that.getranklist(type,a[2]),that.getranklist(type,a[3])])
          .then(function(results){
            that.setData({
              minrankList:results,
              scoreList:[]
            })
            console.log(that.data.minrankList)
            // console.log("执行完成")
            that.queryschool()
          })
         },

    getscore(){
      var a=parseInt(this.data.rank);
      var b=this.data.type
      let that=this
      let p = new Promise(function(resolve, reject){
        wx.cloud.callFunction({
        name :"getscore",
        data:{
          type:b,
          rank:a       
        }
      }).then(res=>{
        // console.log(res)
        let array=res.result.data
        that.setData({
          score:array[0].score
        })
        // console.log(that.data.score)
        resolve("查分数成功")        
      })
      })
      return p
      // console.log(this.data.score) 
     },

   query(){
    let a=parseInt(this.data.rank)
    let b=this.data.type
    if(b=="理科"){
      if(a>180000){
        wx.showModal({
          title: '温馨提醒',
          content: '数据有限，理科仅支持0-18w',
          showCancel:false,
        })

      }else{
        // console.log("成功")
        let p=this.getscore()
        p.then((res)=>{
          // console,log(64);
          // console.log(res);
          this.getinit();      
          });
        this.setData({
          thhidden:false
        }) 
      }

    }else{
      if(a>80000){
        wx.showModal({
          title: '温馨提醒',
          content: '数据有限，文科仅支持0-8w',
          showCancel:false,
        })
      }else{
        // console.log("成功") 
        let p=this.getscore()
        p.then((res)=>{
          // console,log(64);
          // console.log(res);
          this.getinit();      
          });
        this.setData({
          thhidden:false
        }) 
      }
    }
      
    }

  }
})
