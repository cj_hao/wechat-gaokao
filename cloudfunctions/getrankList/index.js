// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

const db=cloud.database();
const _=db.command
// 云函数入口函数
exports.main = async (event, context) => {
  var num=event.num;
  var page=event.page;
  var type=event.type;
  return await db.collection("schoolList")
  .where({
    lscore:_.neq('')
  })
  .orderBy(type,'desc')
  .skip(page).limit(num).get()

}