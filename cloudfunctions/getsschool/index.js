// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db=cloud.database();
const _=db.command
// 云函数入口函数
exports.main = async (event, context) => {
  var rank1=event.rank1;
  var rank2=event.rank2;
  return await db.collection("schoolList")
  .where({
    srank:_.and(_.gte(rank1),_.lte(rank2))
  })
  .orderBy('sscore','desc')
  .get()

}