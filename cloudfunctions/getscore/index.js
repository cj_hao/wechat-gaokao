// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

const db=cloud.database();
const _=db.command
// 云函数入口函数
exports.main = async (event, context) => {
  var type=event.type
  return await db.collection("scoreList")
  .where({
    type:type,
    minrank:_.gte(event.rank),
    maxrank:_.lt(event.rank)
  })
  .limit(5).get()

}